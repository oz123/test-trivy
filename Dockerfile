FROM alpine:3.9

LABEL MAINTAINER Jochen Vetter <jochen.vetter@noris.de>

RUN adduser -D mysql

RUN apk add --no-cache mariadb==10.3.15-r0

COPY run.sh /run.sh

COPY my.cnf /etc/

USER mysql
VOLUME ["/var/lib/mysql"]
ENTRYPOINT ["/run.sh"]
EXPOSE 3306
