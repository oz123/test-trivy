TAG ?=$(shell cat VERSION)
ORG ?= noris-network
IMG := $(ORG)/m:$(TAG)


IMG_TAG := $(IMG):$(TAG)

.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys
cyan, reset = '\033[36m', '\033[0m'
for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%s%-20s%s %s" % (cyan, target, reset, help))
endef

export PRINT_HELP_PYSCRIPT


TRIVY_PPE := https://knqyf263.github.io/trivy-repo/deb
OS := $(shell egrep -c -i 'ubuntu|debian' /etc/os-release)

install-trivy-alpine:
	VERSION=$$(curl --silent "https://api.github.com/repos/knqyf263/trivy/releases/latest" | \
	grep '"tag_name":' | \
	sed -E 's/.*"v([^"]+)".*/\1/' \
	) && curl -L -o trivy.tar.gz https://github.com/knqyf263/trivy/releases/download/v$${VERSION}/trivy_$${VERSION}_Linux-64bit.tar.gz &&  \
	tar zxvf trivy.tar.gz;
	install -m 755 trivy /usr/local/bin

ifneq ($(OS),0)
install-trivy: check-root  ## install trivy to local OS
	apt update && apt-get install -qq -y apt-transport-https gnupg wget git curl software-properties-common
	curl -fsSL ${TRIVY_PPE}/public.key | apt-key add -
	echo deb ${TRIVY_PPE} stretch main | tee -a /etc/apt/sources.list.d/trivy.list
	apt-get update && apt install -qq -y trivy
else
install-trivy:
	@echo "Your OS isn't not supported, see https://github.com/knqyf263/trivy"\
		"for installation instructions for your OS."
endif

install-docker:
	curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
	add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
	apt-get update && apt install -qq -y trivy docker-ce docker-ce-cli containerd.io

check-root:
	@if [ $$(id -u) -ne 0 ]; then echo "you must run this target as root"; exit 1; fi

help:	## print a help file
	@$(PY3) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

build:  ## build the docker image
	docker build --pull --tag $(IMG_TAG) .

lint:   ## static analysis using hadolint
	docker pull hadolint/hadolint:latest-debian
	docker run --rm -i hadolint/hadolint:latest-debian < Dockerfile

scan-cve: ## scan CVEs using trivy
	# Works only after a successfully build and trivy installed
	trivy $(IMG_TAG)

run:    ## run a local copy
	docker run -it --rm $(IMG_TAG)

run-as-service:
	docker run -d --restart always $(IMG_TAG)

clean:  ## clean images
	docker rmi `docker images -q $(IMG_TAG)`

scan-or-delete:  ## scan for CVE and if fail delete the image from the registry
	trivy --exit-code 1 --auto-refresh ${IMAGE}:${VERSION} || { ${MAKE} delete; exit 1; }


delete: REGISTRY_ID := $(shell curl --header "PRIVATE-TOKEN: ${APIKEY}" https://gitlab.noris.net/api/v4/projects/${CI_PROJECT_ID}/registry/repositories/ | jq '.[0].id ')
delete:  ## delete an image from gitlab registry using and API call
	curl -X DELETE --header "PRIVATE-TOKEN: ${APIKEY}" "https://gitlab.noris.net/api/v4/projects/${CI_PROJECT_ID}/registry/repositories/${REGISTRY_ID}/tags/${VERSION}";

