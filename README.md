MySQL noris
===============================

A MySQL image for noris

Usage
-----

```bash
make build
make run
```

Params
------

You can specify the following params when you call make:

- Specify a TAG for your Image, default: latest

```bash
make build tag=1.0
```

Configuration
-------------